from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from django.urls import path
#
# from message.consumers import ChatConsumer, ThreadSocketConsumer
# from message.middlewares import JWTAuthMiddleware
# from social.consumers import SocialSocketConsumer

application = ProtocolTypeRouter({
    # Empty for now (http->django views is added by default)
    # "websocket": JWTAuthMiddleware(
    #     URLRouter([
    #         # URLRouter just takes standard Django path() or url() entries.
    #         path("chat/stream/", ChatConsumer),
    #         path("chat/threads/", ThreadSocketConsumer),
    #         path("notification/social/", SocialSocketConsumer),
    #     ]),
    # ),
})