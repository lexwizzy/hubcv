from django.contrib.auth import get_user_model
from django.db import models

# Create your models here.
from stream_django import feed_manager
from stream_django.activity import Activity

User = get_user_model()


class HcFollowingSkills(models.Model):
    user = models.ForeignKey(User, db_index=True, related_name="user_following_skills", on_delete=models.CASCADE)
    skill = models.CharField(max_length=255)

    def __str__(self):
        return self.skill

    class Meta:
        verbose_name = "Following Skill"
        db_table = "hc_following_skill"
        unique_together = (("user", "skill"), )


class HcFollow(models.Model, Activity):
    follower = models.ForeignKey(User, related_name="who_followed", on_delete=models.CASCADE,
                                 help_text='Represent the user that followed another user(following)')
    following = models.ForeignKey(User, related_name="who_is_following", on_delete=models.CASCADE,
                                  help_text='Represent the user that was followed')
    verb = models.CharField(max_length=255, null=True, blank=True, default="followed you")
    followed_at = models.DateTimeField(auto_now_add=True, db_index=True)

    @property
    def activity_author_feed(self):
        return "timeline_aggregated"

    @property
    def activity_actor_attr(self):
        return self.follower

    @classmethod
    def activity_related_models(cls):
        # user,target
        return ['follower', 'following']

    @property
    def activity_verb(self):
        return self.verb

    @property
    def activity_object_attr(self):
        return self

    @property
    def activity_time(self):
        return self.followed_at

    @property
    def activity_notify(self):
        target_feed = feed_manager.get_notification_feed(self.following_id)
        return [target_feed]

    class Meta:
        verbose_name = "Follow"
        db_table = 'hc_follow'
        ordering = ("-followed_at",)
        unique_together = [("follower", "following")]


User.add_to_class('following', models.ManyToManyField('self', through=HcFollow, related_name="followers",
                                                      symmetrical=False))