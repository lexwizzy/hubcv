import json

from django.contrib.auth import get_user_model
from drf_haystack.filters import HaystackHighlightFilter, HaystackAutocompleteFilter, HaystackFacetFilter
from drf_haystack.mixins import FacetMixin
from drf_haystack.viewsets import HaystackViewSet
from rest_framework import permissions
from rest_framework.decorators import list_route
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from company.models import UsCompany
from core.serializers import MainSearchSerializer, MainSearchFacetSerializer, MainMemberSearchSerializer
from event.models import UsEvent
from post import models as post_models
from college import models as college_models


User = get_user_model()


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 30
    page_size_query_param = 'page_size'
    max_page_size = 1000


class MainSearchView(FacetMixin, HaystackViewSet):

    # `index_models` is an optional list of which models you would like to include
    # in the search result. You might have several models indexed, and this provides
    # a way to filter out those of no interest for this particular view.
    # (Translates to `SearchQuerySet().models(*index_models)` behind the scenes.
    index_models = [UsEvent, User, college_models.UsCollege, UsCompany, post_models.UsPost]

    serializer_class = MainSearchSerializer
    # permission_classes = (permissions.IsAuthenticated,)
    facet_query_params_text = "params"
    facet_serializer_class = MainSearchFacetSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [HaystackHighlightFilter, HaystackAutocompleteFilter]

    def get_index_model(self):
        request = self.request
        if request.query_params.get("type") == "mem":
            return [User]
        elif request.query_params.get("type") == "com":
            return [UsCompany]
        elif request.query_params.get("type") == "col":
            return [college_models.UsCollege]
        elif request.query_params.get("type") == "post":
            return [post_models.UsPost]
        return self.index_models

    def get_queryset(self, index_models=[]):
        """
        Get the list of items for this view.
        Returns ``self.queryset`` if defined and is a ``self.object_class``
        instance.

        @:param index_models: override `self.index_models`
        """
        if self.queryset is not None and isinstance(self.queryset, self.object_class):
            queryset = self.queryset.all()
        else:
            queryset = self.object_class()._clone()
            if len(index_models):
                queryset = queryset.models(*index_models)
            elif len(self.index_models):
                queryset = queryset.models(*self.index_models)

            if self.request.query_params.get("text"):
                queryset = queryset.filter(content__startswith=self.request.query_params.get("text"))
        return queryset

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset(self.get_index_model()))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True, context={"request": request})
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True, context={"request": request})

        return Response(serializer.data)

    @list_route(methods=["get"], url_path="facets")
    def facets(self, request):
        """
        Sets up a list route for ``faceted`` results.
        This will add ie ^search/facets/$ to your existing ^search pattern.
        """
        queryset = self.filter_facet_queryset(self.filter_queryset(self.get_queryset(self.get_index_model())))

        for facet in request.query_params.getlist(self.facet_query_params_text):

            if ":" not in facet:
                continue

            field, value = facet.split(":", 1)
            if value:
                queryset = queryset.narrow('%s:"%s"' % (field, queryset.query.clean(value)))

        serializer = self.get_facet_serializer(queryset.facet_counts(), objects=queryset, many=False)
        return Response(serializer.data)


class MainMemberSearchView(HaystackViewSet):

    # `index_models` is an optional list of which models you would like to include
    # in the search result. You might have several models indexed, and this provides
    # a way to filter out those of no interest for this particular view.
    # (Translates to `SearchQuerySet().models(*index_models)` behind the scenes.
    index_models = [User]

    serializer_class = MainMemberSearchSerializer
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class = StandardResultsSetPagination
    filter_backends = [HaystackHighlightFilter, HaystackAutocompleteFilter]

    def get_index_model(self):
        request = self.request
        return [User]

    def get_queryset(self, index_models=[]):
        """
        Get the list of items for this view.
        Returns ``self.queryset`` if defined and is a ``self.object_class``
        instance.

        @:param index_models: override `self.index_models`
        """
        if self.queryset is not None and isinstance(self.queryset, self.object_class):

            queryset = self.queryset.all()
        else:
            queryset = self.object_class()._clone()
            if len(index_models):
                queryset = queryset.models(*index_models)
            elif len(self.index_models):
                queryset = queryset.models(*self.index_models)

            if self.request.query_params.get("text"):
                queryset = queryset.filter(content__startswith=self.request.query_params.get("text"))
        return queryset

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset(self.get_index_model()))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True, context={"request": request})
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True, context={"request": request})

        return Response(serializer.data)