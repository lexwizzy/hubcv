from django.contrib import admin
from authentication import models
# Register your models here.


class HcUsersAdmin(admin.ModelAdmin):
    list_display = ["id", "email",  "phone"]

admin.site.register(models.HcUser, HcUsersAdmin)