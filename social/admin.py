from django.contrib import admin
from social import models
# Register your models here.


admin.site.register(
    models.HcFollow,
    list_display=["id", "follower", "following"]
)

admin.site.register(
    models.HcFollowingSkills,
    list_display=["id", "user", "skill"]
)