from rest_framework import serializers
from skills import models


class HcSkillSetSerializer(serializers.ModelSerializer):

    class Meta:
        fields = "__all__"
        model = models.HcSkillSets
