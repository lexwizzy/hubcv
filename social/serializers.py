from rest_framework import serializers
from social import models
from authentication import serializers as user_serializers


class HcFollowingSkillsSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.HcFollowingSkills
        fields = "__all__"


class HcFollowSerializer(serializers.ModelSerializer):

    follower_profile = serializers.SerializerMethodField(read_only=True)
    following_profile = serializers.SerializerMethodField(read_only=True)

    def get_follower_profile(self,obj):
        if obj.follower:
            return user_serializers.HcUserSerializer(obj.follower).data
        return None

    def get_following_profile(self, obj):
        if obj.following:
            return user_serializers.HcUserSerializer(obj.following).data

        return None

    class Meta:
        fields = "__all__"
        model = models.HcFollow