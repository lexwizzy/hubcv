from django.urls import path

from authentication import api

app_name = "authentication"

urlpatterns = [
    path("api/v2/send-otp/", api.HcWebSendOTPApiView.as_view()),
    path("api/v2/validate-otp/", api.HcWebValidateOTPApiView.as_view()),
    path("api/v2/resend-otp/", api.HcWebSendOTPApiViewV2.as_view()),
    path("api/v2/on-boarding/", api.HcOnboardingAPIView.as_view()),
]