from django.db import models

# Create your models here.


class HcSkillsCategory(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Skill Category"
        db_table = "hc_skill_category"


class HcSkillsSubCategory(models.Model):
    name = models.CharField(max_length=255)
    category = models.ForeignKey(HcSkillsCategory, related_name="sub_categories", on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Skill SubCategory"
        db_table = "hc_skill_sub_category"


class HcSkillSets(models.Model):
    name = models.CharField(max_length=255)
    category = models.ForeignKey(HcSkillsCategory, related_name="category_skills", on_delete=models.SET_NULL, null=True)
    subcategory = models.ForeignKey(HcSkillsSubCategory, related_name="sub_category_skills",
                                    on_delete=models.SET_NULL, null=True)
    initials = models.CharField(max_length=2, null=True, blank=True)
    # image = models.ImageField(upload_to=skill_cover_image, null=True, blank=True)

    class Meta:
        verbose_name = "Skill Set"
        db_table = "hc_skill_sets"
