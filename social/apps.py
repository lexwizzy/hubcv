from django.apps import AppConfig


class SocialConfig(AppConfig):
    name = 'social'
    verbose_name = "Social Activities"

    def ready(self):
        import social.signals
