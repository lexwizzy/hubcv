from django.urls import path, include
from skills import api

app_name = "skills"

urlpatterns = [
    path("skill-search/v1/", api.HcSkillSetAPIView.as_view()),
    path("skill-category/v1/", api.HcSkillsCategory.as_view()),
    path("skill-subcategory/v1/", api.HcSkillsSubCategory.as_view()),
    path("skill-by-category/v1/", api.HcSkillByCategory.as_view()),
    path("skill-by-subcategory/v1/", api.HcSkillsBySubCategory.as_view()),
]