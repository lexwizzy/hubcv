from django.conf import settings


def send_otp(mobile):
    import urllib.request
    import ast

    with urllib.request.urlopen(
                                            'https://2factor.in/API/V1/' + settings.TWO_FACTOR_SMS_API_KEY + '/SMS/91'
                            + mobile + "/AUTOGEN") as response:
        html = ast.literal_eval(response.read().decode())
        return html