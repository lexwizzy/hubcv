from django.conf import settings
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from authentication import serializers
from authentication.utils import send_otp
from rest_framework.parsers import FormParser, MultiPartParser

UsUser = get_user_model()


class HcWebSendOTPApiView(APIView):
    serializer_class = serializers.HcSendOTPSerializer

    def post(self, request):
        mobile = request.data.get("mobile")
        if UsUser.objects.filter(phone=mobile).exists():
            return Response({"message": "Phone number already in use. "
                                        "Please use a different number and try again"},
                            status=status.HTTP_403_FORBIDDEN)

        html = send_otp(mobile)
        return Response(html)


class HcWebSendOTPApiViewV2(APIView):
    serializer_class = serializers.HcSendOTPSerializer

    def post(self, request):
        mobile = request.data.get("mobile")
        html = send_otp(mobile)
        return Response(html)


class HcWebValidateOTPApiView(APIView):
    serializer_class = serializers.HcValidateOtpSerializer

    def post(self, request):
        import urllib.request
        import ast

        details = request.data.get("details")
        otp = request.data.get("otp")

        with urllib.request.urlopen('https://2factor.in/API/V1/'+settings.TWO_FACTOR_SMS_API_KEY+'/SMS/VERIFY/'+
                                            details +"/"+ otp) as response:
            html = ast.literal_eval(response.read().decode())
            return Response(html)


class HcOnboardingAPIView(APIView):
    serializer_class = serializers.HcOnBoardingSerializer
    parser_classes = (FormParser, MultiPartParser)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        if request.query_params.get("action") != 'validate':
            serializer.save()
        return Response(serializer.data)
