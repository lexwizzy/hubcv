from rest_framework.generics import ListCreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from skills import models
from skills import serializers


class HcSkillSetAPIView(ListCreateAPIView):
    queryset = models.HcSkillSets.objects.all()
    # permission_classes = (
    #     permissions.IsAuthenticated,
    # )
    serializer_class = serializers.HcSkillSetSerializer

    def list(self, request, *args, **kwargs):
        queryset = models.HcSkillSets.objects.filter(name__icontains=self.request.GET.get("q"))[:20]
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)


class HcSkillsCategory(APIView):

    def get(self, request):
        queryset = models.HcSkillsCategory.objects.all()
        data = []
        for d in queryset:
            data.append({"id": d.id, "name": d.name})
        return Response(data)


class HcSkillsSubCategory(APIView):

    def get(self, request, *args, **kwargs):
        queryset = models.HcSkillsSubCategory.objects.all()
        data = []
        for d in queryset:
            data.append({"id": d.id, "name": d.name})
        return Response(data)


class HcSkillByCategory(APIView):
    serializer_class = serializers.HcSkillSetSerializer

    def get(self, request):
        queryset = models.HcSkillSets.objects.filter(category=self.request.GET.get("c"))
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)


class HcSkillsBySubCategory(APIView):
    serializer_class = serializers.HcSkillSetSerializer

    def get(self, request):
        queryset = models.HcSkillSets.objects.filter(subcategory=self.request.GET.get("c"))
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)