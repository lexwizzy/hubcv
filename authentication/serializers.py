import os

from PIL import Image
from django.db import transaction
from rest_framework import serializers
from authentication import models
from social.models import HcFollowingSkills, HcFollow


class HcSendOTPSerializer(serializers.Serializer):
    mobile = serializers.CharField(max_length=255, read_only=False)


class HcValidateOtpSerializer(serializers.Serializer):
    details = serializers.CharField()
    otp = serializers.IntegerField()


class HcUserProfileSerializer(serializers.ModelSerializer):

    def validate_avatar(self, image):

        if image:
            img = Image.open(image)
            fileName, fileExtension = os.path.splitext(image.name)
            if not fileExtension in ['.jpeg', '.jpeg', '.png', '.jpg', ".JPG", ".PNG", ".JPEG"]:
                raise serializers.ValidationError(_("Please use a JPEG or PNG image."))
            # validate file size
            if len(image) > 5000000:
                raise serializers.ValidationError(_('Image file too large ( maximum 5mb )'))
                # else:
                #     raise forms.ValidationError(_("Couldn't read uploaded image"))
        return image

    class Meta:
        model = models.HcUserProfile
        fields = ("first_name", "last_name", "country",
                  "avatar", "state", "industry", "city", "profile_type")


class HcUserSerializer(serializers.ModelSerializer):
    profile = HcUserProfileSerializer()

    def validate_email(self, email):
        if not self.instance and models.HcUser.objects.filter(email__iexact=email).exists():
            raise serializers.ValidationError("Email already exist")
        elif self.instance:
            if self.instance.email != email:
                if models.HcUser.objects.filter(email__iexact=email).exists():
                    raise serializers.ValidationError("Email already exist")
        return email

    def validate_username(self, username):
        try:
            if models.HcUser.objects.filter(~Q(pk=self.context.get("request").user.id),
                                            username__iexact=username).exists():
                raise serializers.ValidationError("User with this Username already exists.")
        except:
            ''' catch error while creating bulk user, request object is not passed'''
            if models.HcUser.objects.filter(username__iexact=username).exists():
                raise serializers.ValidationError("User with this Username already exists.")

        return username

    def validate_phone(self, phone):
        if not self.instance and models.HcUser.objects.filter(phone=phone).exists():
            raise serializers.ValidationError("Phone number already exist")
        elif self.instance:
            if str(self.instance.phone) != str(phone):
                if models.HcUser.objects.filter(phone=phone).exists():
                    raise serializers.ValidationError("Phone number already exist")
        return phone

    class Meta:
        model = models.HcUser
        fields = ["profile", "email", "phone", "username"]


class HcOnBoardingSerializer(serializers.ModelSerializer):
    skills_to_followed = serializers.JSONField(default=[])
    profile = HcUserProfileSerializer()
    people_followed = serializers.JSONField(default=[])
    password = serializers.CharField(max_length=255, write_only=True)

    def create(self, validated_data):
        print(validated_data)
        with transaction.atomic():

            user = models.HcUser.objects.create(
                email=validated_data.get("email"),
                phone=validated_data.get("phone"),
                username=validated_data.get("username")
            )
            user.set_password(validated_data.get("password"))
            user.save()
            p = validated_data.get("profile")
            p["user"] = user
            profile = models.HcUserProfile.objects.create(
                **p
            )

            # Save followed skills

            skills = []
            for s in validated_data.get("skills_to_followed"):
                skills.append(HcFollowingSkills(skill=s, user=user))

            HcFollowingSkills.objects.bulk_create(skills)

            # Save followed People
            people = []
            for s in validated_data.get("people_followed"):
                u = models.HcUser.objects.get(pk=s)
                people.append(HcFollow(follower=user, following=u))

            HcFollow.objects.bulk_create(people)
        return user

    class Meta:
        model = models.HcUser
        fields = ["profile", "email", "phone", "username", "password", "skills_to_followed", "people_followed"]

