from itertools import chain

from django.contrib.auth import get_user_model
from drf_haystack.serializers import HaystackSerializer, HaystackFacetSerializer
from authentication.search_indexes import UsUserIndex
from college.search_indexes import UsCollegeIndex
from company import models as company_models
from college import models as college_models
from rest_framework import serializers
from company.search_indexes import UsCompanyIndex
from post.models import UsPost
from post.search_indexes import UsPostIndex
from authentication.serializers import UsSimpleUserDetailsV1Serializer
UsUser = get_user_model()


class MainMemberSearchSerializer(HaystackSerializer):
    id = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):

        super(MainMemberSearchSerializer, self).__init__(*args, **kwargs)
        self.request = self.context.get("request")

    def get_status(self, obj):
        if self.request.query_params.get("type") == "student":
            s = college_models.UsStudent.objects.filter(user=obj.instance_id)
        elif self.request.query_params.get("type") == "faculty":
            s = college_models.UsFaculty.objects.filter(user=obj.instance_id)
        else:
            s = college_models.UsAdmin.objects.filter(user=obj.instance_id)

        if s.exists():
            return s[0].status
        return 1

    def get_id(self, obj):
        return obj.instance_id
    # def get_status(self, obj):
    #
    #     return

    class Meta:
        index_classes = [UsUserIndex]
        fields = ["first_name", "last_name", "headline",
                  "phone", "name", "avatar", "id", "status", ]

    def to_representation(self, instance):
        """
        If we have a serializer mapping, use that.  Otherwise, use standard serializer behavior
        Since we might be dealing with multiple indexes, some fields might
        not be valid for all results. Do not render the fields which don't belong
        to the search result.
        """
        if self.Meta.serializers:
            ret = self.multi_serializer_representation(instance)
        else:
            ret = super(HaystackSerializer, self).to_representation(instance)
            prefix_field_names = len(getattr(self.Meta, "index_classes")) > 1
            current_index = self._get_index_class_name(type(instance.searchindex))
            for field in self.fields.keys():
                orig_field = field
                if prefix_field_names:
                    parts = field.split("__")
                    if len(parts) > 1:
                        index = parts[0][1:]  # trim the preceding '_'
                        field = parts[1]
                        if index == current_index:
                            ret[field] = ret[orig_field]
                        del ret[orig_field]
                elif field not in chain(instance.searchindex.fields.keys(), self._declared_fields.keys()):
                    del ret[orig_field]

        # include the highlighted field in either case
        if getattr(instance, "highlighted", None):
            ret["highlighted"] = instance.highlighted.get("text")[0]
        return ret


class MainSearchSerializer(HaystackSerializer):
    id = serializers.SerializerMethodField()
    is_following = serializers.SerializerMethodField()
    followers_count = serializers.SerializerMethodField()
    view_count = serializers.SerializerMethodField()
    like_count = serializers.SerializerMethodField()
    comment_count = serializers.SerializerMethodField()
    file_type = serializers.SerializerMethodField()
    user = serializers.SerializerMethodField()
    # target = serializers.SerializerMethodField()

    # def get_target(self, obj):
    #     return utils.replacenth(obj.id, ".", ":", 2)
    #

    def get_file_type(self, obj):
        if obj.model == UsPost:
            f = obj.object.file.first()
            if f:
                return f.file_type
        return None

    def get_view_count(self, obj):
        if obj.model == UsPost:
            return obj.object.view_count
        return 0

    def get_user(self, obj):
        if obj.model == UsPost:
            return UsSimpleUserDetailsV1Serializer(obj.object.created_by).data
        return None

    def get_comment_count(self, obj):
        if obj.model == UsPost:
            return obj.object.comment.count()
        return 0

    def get_like_count(self, obj):
        if obj.model == UsPost:
            return obj.object.likes.count()
        return 0

    def get_id(self, obj):
        return obj.instance_id

    def get_followers_count(self, obj):
        try:
            if obj.model == UsUser:
                return obj.object.followers_count
            elif obj.model == college_models.UsCollege:
                return obj.object.followers_count
            else:
                return 0
        except:
            return 0

    def get_is_following(self,obj):
        request = self.context.get("request")
        if request:
            try:
                if obj.model == UsUser and request.user.id != obj.instance_id:
                    return request.user.is_a_follower(obj.instance_id)
                elif obj.model == college_models.UsCollege:
                    return obj.object.is_a_follower(request.user.id)
                # else:
                #     return obj.object.is_a_follower(request.user.id)
            except:
                return False

        return False

    class Meta:
        # The `index_classes` attribute is a list of which search indexes
        # we want to include in the search.
        index_classes = [UsUserIndex, UsCollegeIndex, UsCompanyIndex, UsPostIndex]

        # index_classes = [college_index.UsStudentIndex]
        # The `fields` contains all the fields we want to include.
        # NOTE: Make sure you don't confuse these with model attributes. These
        # fields belong to the search index!
        fields = ["first_name", "last_name", "headline", "college_name", "logo",
                  "u_type", "name", "avatar", "city", "state", "country",
                  "id", "status", "followers_count", "title", "like_count", "view_count", "comment_count", "file_type"]

    def to_representation(self, instance):
        """
        If we have a serializer mapping, use that.  Otherwise, use standard serializer behavior
        Since we might be dealing with multiple indexes, some fields might
        not be valid for all results. Do not render the fields which don't belong
        to the search result.
        """
        if self.Meta.serializers:
            ret = self.multi_serializer_representation(instance)
        else:
            ret = super(HaystackSerializer, self).to_representation(instance)
            prefix_field_names = len(getattr(self.Meta, "index_classes")) > 1
            current_index = self._get_index_class_name(type(instance.searchindex))
            for field in self.fields.keys():
                orig_field = field
                if prefix_field_names:
                    parts = field.split("__")
                    if len(parts) > 1:
                        index = parts[0][1:]  # trim the preceding '_'
                        field = parts[1]
                        if index == current_index:
                            ret[field] = ret[orig_field]
                        del ret[orig_field]
                elif field not in chain(instance.searchindex.fields.keys(), self._declared_fields.keys()):
                    del ret[orig_field]

        # include the highlighted field in either case
        if getattr(instance, "highlighted", None):
            ret["highlighted"] = instance.highlighted.get("text")[0]
        return ret


class MainSearchFacetSerializer(HaystackFacetSerializer):

    serialize_objects = True  # Setting this to True will serialize the
                               # queryset into an `objects` list. This
                               # is useful if you need to display the faceted
                               # results. Defaults to False.
    class Meta:
        index_classes = [UsUserIndex]

        fields = ["first_name", "last_name",
                  "college",
                  "skills_category", "skills",
                  "headline" "city", "state", "country"]
        field_options = {
            # "file_type": {},
            # "post_type":{},
            "skills_category": {},
            "skills": {},
            "college": {},
            "course": {},
            "city": {},
            "state": {},

        }