from django.contrib.auth import get_user_model
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from stream_django import feed_manager

User = get_user_model()


@receiver(post_save, sender=User.followers.through)
def post_follow(sender, instance, **kwargs):
    # Increase the followers count of the following
    instance.following.followers_count = instance.following.followers.count()
    instance.following.save()
    # Increase the following count of the user that followed another user
    instance.follower.following_count = instance.follower.following.count()
    instance.follower.save()
    f = feed_manager.follow_user(instance.follower.id, instance.following.id)
    # notify_user(instance.follower, instance.verb, instance, instance.following)
    # send_in_app_notification("social", instance.following.id)


@receiver(post_delete, sender=User.followers.through)
def followers_change(sender, instance, **kwargs):
    # Increase the followers count of the following
    instance.following.followers_count = instance.following.followers.count()
    instance.following.save()
    # Increase the following count of the user that followed another user
    instance.follower.following_count = instance.follower.following.count()
    instance.follower.save()
    feed_manager.unfollow_user(instance.follower.id, instance.following.id)