import os

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from core.storage_backends import PublicMediaStorage


def avatar_upload_path(instance,filename):
    filename, ext = os.path.splitext(filename)
    filename = "{0}{1}".format(int(timezone.now().timestamp()), ext)
    return "user/upload/avatar/{0}".format(filename)


def cover_upload_path(instance,filename):
    filename, ext = os.path.splitext(filename)
    filename = "{0}{1}".format(int(timezone.now().timestamp()), ext)
    return "user/upload/cover/{0}".format(filename)


class HcUser(AbstractUser):
    # USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["email", "phone"]
    phone = models.CharField(max_length=13, unique=True)
    email = models.EmailField(_('email address'), max_length=254, unique=True, db_index=True)
    username = models.CharField(_('username'), max_length=500, blank=True, unique=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designated whether the user can log into this admin '
                                               'site.'))
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.'))

    def __str__(self):
        try:
            return "{0} {1}-{2}".format(self.profile.first_name, self.profile.last_name,self.id)
        except:
            return self.email

    def get_full_name(self):
        return "{0} {1}". format(self.profile.first_name, self.profile.last_name if self.profile.last_name else "")

    def get_avatar(self):
        return self.profile.avatar

    class Meta:
        db_table = "hc_user"
        verbose_name = "User"


class HcUserProfile(models.Model):
    PROFILE_TYPE = (("student", "Student"), ("faculty", "Faculty"), ("college_admin", "College Admin"),
                 ("employee", "Employee"), ("hr", "HR"), ("company_admin", "Company Admin"))

    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255, null=True, blank=True, default="")
    user = models.OneToOneField(HcUser, related_name="profile", on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to=avatar_upload_path, null=True, blank=True)
    dob = models.DateField(null=True, blank=True)
    gender = models.CharField(max_length=1, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    about = models.CharField(max_length=2550, null=True, blank=True)
    profile_type = models.CharField(choices=PROFILE_TYPE, max_length=50, null=True)
    is_online = models.BooleanField(default=False, help_text="User online status")
    city = models.CharField(max_length=100, null=True)
    state = models.CharField(max_length=100, null=True)
    country = models.CharField(max_length=100, null=True)
    cover_image = models.ImageField(upload_to=cover_upload_path, storage=PublicMediaStorage(), null=True, blank=True)
    industry = models.CharField(null=True,  max_length=100)

    def __str__(self):
        try:
            return "{0} {1}-{2}".format(self.profile.first_name, self.profile.last_name,self.id)
        except:
            return self.email

    def get_full_name(self):
        return "{0} {1}". format(self.first_name, self.last_name if self.last_name else "")

    def get_avatar(self):
        return self.avatar

    class Meta:
        db_table = "hc_user_profile"
        verbose_name = "User Profile"